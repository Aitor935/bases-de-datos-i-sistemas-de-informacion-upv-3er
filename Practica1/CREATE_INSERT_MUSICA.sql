--CREATE DATABASE MUSICA;
----
Use musica;
CREATE TABLE ARTISTA
(
	dni varchar(10) PRIMARY KEY,
	nombre varchar(30),
	edad smallint

)

CREATE TABLE GRUPO(
	cod varchar(3) PRIMARY KEY,
	nombre varchar(30) NOT NULL,
	fecha datetime,
pais varchar(10),
estilo varchar(10)

)

CREATE TABLE PERTENECE(
dni varchar(10) ,
cod varchar(3),
funcion varchar(15),
PRIMARY KEY(dni,cod),
FOREIGN KEY(dni) REFERENCES ARTISTA(dni) ON UPDATE CASCADE,
FOREIGN KEY(cod) REFERENCES GRUPO(cod) ON UPDATE CASCADE

)


CREATE TABLE COMPANYIA(
	cod integer PRIMARY KEY,
	nombre varchar(30) NOT NULL,
	dir varchar(30),
	fax varchar(15),
tfno varchar(15)
)


CREATE TABLE DISCO(
cod varchar(3) PRIMARY KEY,
	nombre varchar(30),
fecha datetime,
	cod_comp integer REFERENCES COMPANYIA(cod) on update cascade,
	cod_gru varchar(3) NOT NULL  REFERENCES GRUPO (cod) on update cascade


)

CREATE TABLE CANCION(
cod varchar(3) PRIMARY KEY,
	titulo varchar(30) NOT NULL,
	duracion decimal(4,2)
)

CREATE TABLE ESTA(
	can varchar(3),
	cod varchar(3),
	PRIMARY KEY (can,cod),
FOREIGN KEY(can) REFERENCES CANCION(cod) ON UPDATE CASCADE,
FOREIGN KEY(cod) REFERENCES DISCO(cod) ON UPDATE CASCADE

)


CREATE TABLE CLUB(
cod varchar(3) PRIMARY KEY,
	nombre varchar(30) NOT NULL,
	sede varchar(30),
num_socios integer,
num_socias integer,
cod_gru varchar(3) REFERENCES GRUPO(cod) ON UPDATE CASCADE

);
--Inserción de datos en Artista.
--
insert into artista(dni,nombre,edad) values ('1111111111','Adrian Lee',45);
insert into artista(dni,nombre,edad) values ('1111111112','Adam Clayton',42);
insert into artista(dni,nombre,edad) values ('1111111113','Bono',42);
insert into artista(dni,nombre,edad) values ('1111111114','C. Burchill',20);
insert into artista(dni,nombre,edad) values ('1111114444','Carlos Torero',28);
insert into artista(dni,nombre,edad) values ('2345678444','Edge',21);
insert into artista(dni,nombre,edad) values ('3232456547','Phil Collins',56);
insert into artista(dni,nombre,edad) values ('3333567898','Santiago Auseron',28);
insert into artista(dni,nombre,edad) values ('3454677777','Jim Kerr',26);
insert into artista(dni,nombre,edad) values ('4444444444','Larry Jr.Mullen',32);
insert into artista(dni,nombre,edad) values ('4454321111','Luis Auseron',22);
insert into artista(dni,nombre,edad) values ('5454532222','Paul Young',46);
insert into artista(dni,nombre,edad) values ('5555678976','Enrique Sierra',52);
insert into artista(dni,nombre,edad) values ('5556787777','J.L. Giménez',63);
insert into artista(dni,nombre,edad) values ('5656378999','Soledad Giménez',43);
insert into artista(dni,nombre,edad) values ('6666667885','Nacho Maño',42);
insert into artista(dni,nombre,edad) values ('7654323234','P. van Hooke',61);
insert into artista(dni,nombre,edad) values ('7876543428','Tony Banks',63);
insert into artista(dni,nombre,edad) values ('8884566666','M. Rutherford',44);

--
--Inserción de datos en Grupo
--
insert into grupo(cod,nombre,fecha,pais,estilo) values ('1U2','U2','01/01/1977','Inglaterra','POP-ROCK');
insert into grupo(cod,nombre,fecha,pais,estilo) values ('2SM','Simple Minds','09/02/1979','Inglaterra','NEW WAVE');
insert into grupo(cod,nombre,fecha,pais,estilo) values ('3MM','Mike + The Mechanics','06/04/1988','Inglaterra','POP');
insert into grupo(cod,nombre,fecha,pais,estilo) values ('4GN','Genesis','10/10/1975','Inglaterra','POP');
insert into grupo(cod,nombre,fecha,pais,estilo) values ('5PI','Presuntos Implicados','01/11/1985','España','POP');
insert into grupo(cod,nombre,fecha,pais,estilo) values ('6RF','Futura','07/01/1980','España','NEW WAVE');

--
--Inserción de datos en Club
--
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('BMD','The best mind','24, Homeround',1413,'2SM',null);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('BNC','Bonoculture','12, East Av.',129,'1U2',129);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('CH2','Che U2','C/Almussafes 59',239,'1U2',242);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('FGE','Fanaticgens','Av. H. Dominicos 155',12002,'4GN',1000);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('FNM','FanMike','Beverly Hills 90210',11,'3MM',12);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('FUT','Futuristas','C/Alboraya 10',9850,'6RF',985);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('GNF','Genefans','C/Visitacion 34',23412,'4GN',23412);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('IMP','Implicado','Torrejon de Ardoz 12',25,'5PI',340);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('JBO','Jardin Botanico','203,Valencia 46004',357,'6RF',10);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('LCU','Los Culpables','C/Maria Cristina 67',355,'5PI',69);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('MAC','Machines','Calle 3, Lab 3',7789,'3MM',120);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('MEF','Mentes Fuertes','Ramon y Cajal 14',1984,'2SM',256);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('PRS','Presuntos','C/Albacete 12, bajo',237,'5PI',238);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('TRM','Troglominds','C/Lepe 22',999,'2SM',999);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('U2Y','u2foryou','23, 11th Street',1700,'1U2',166);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('WTF','Waterfront','C/Martin Blas 22',234,'2SM',2);
insert into Club(cod,nombre,sede,num_socios,cod_gru,num_socias) values ('ZZM','Zoomania','33, Abbey Road',2508,'1U2',123);
--
--Inserción de datos en Pertenece
--
insert into Pertenece(dni,cod,funcion) values ('1111111111','3MM','teclado');
insert into Pertenece(dni,cod,funcion) values ('1111111112','1U2','bajo');
insert into Pertenece(dni,cod,funcion) values ('1111111113','1U2','voz');
insert into Pertenece(dni,cod,funcion) values ('1111111114','2SM','guitarra');
insert into Pertenece(dni,cod,funcion) values ('1111114444','6RF','batería');
insert into Pertenece(dni,cod,funcion) values ('2345678444','1U2','guitarra');
insert into Pertenece(dni,cod,funcion) values ('3232456547','4GN','voz');
insert into Pertenece(dni,cod,funcion) values ('3333567898','6RF','voz');
insert into Pertenece(dni,cod,funcion) values ('3454677777','2SM','voz');
insert into Pertenece(dni,cod,funcion) values ('4444444444','1U2','batería');
insert into Pertenece(dni,cod,funcion) values ('4454321111','6RF','bajo');
insert into Pertenece(dni,cod,funcion) values ('5454532222','3MM','voz');
insert into Pertenece(dni,cod,funcion) values ('5555678976','6RF','guitarra');
insert into Pertenece(dni,cod,funcion) values ('5556787777','5PI','guitarra');
insert into Pertenece(dni,cod,funcion) values ('5656378999','5PI','voz');
insert into Pertenece(dni,cod,funcion) values ('6666667885','5PI','bajo');
insert into Pertenece(dni,cod,funcion) values ('7654323234','3MM','batería');
insert into Pertenece(dni,cod,funcion) values ('7876543428','4GN','teclado');
insert into Pertenece(dni,cod,funcion) values ('8884566666','3MM','bajo');
insert into Pertenece(dni,cod,funcion) values ('8884566666','4GN','bajo');

--
--Inserción de datos en Companyia
--
insert into Companyia(cod,nombre,dir,fax,tfno) values (1,'Island','67, JB St.','78782222','72724444');
insert into Companyia(cod,nombre,dir,fax,tfno) values (2,'ARIOLA','Aragon 204','913667889','913667890');
insert into Companyia(cod,nombre,dir,fax,tfno) values (3,'WEA','L Hoyos 42','932401212','932401213');
insert into Companyia(cod,nombre,dir,fax,tfno) values (4,'Virgin','2,23th St.','20812445','20812446');
insert into Companyia(cod,nombre,dir,fax,tfno) values (5,'ATLANTIC','12, E St.','5481223','5482312');
insert into Companyia(cod,nombre,dir,fax,tfno) values (6,'PoliDiscos','Cami de Vera','3870001','3870000');
insert into Companyia(cod,nombre,dir,fax,tfno) values (7,'PoliDiscos','Polynesia St.','942380540','942380522');

--
--Inserción de datos en Disco
--
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('1','October','12/10/1981','1','1U2');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('10','Word of mouth','07/05/1991','5','3MM');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('11','We can''t dance','02/02/1991','5','4GN');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('12','Invisible touch','03/03/1986','5','4GN');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('13','Seconds out','08/08/1986','5','4GN');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('14','De sol a sol','08/01/1987','3','5PI');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('15','Ser de agua','05/02/1991','3','5PI');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('16','Alma de blues','03/02/1989','3','5PI');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('17','La ley del desierto','02/03/1984','2','6RF');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('18','La canción de Jperro','03/04/1987','2','6RF');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('2','Zooropa','10/08/1994','1','1U2');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('3','The unforgettable fi','07/03/1983','1','1U2');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('4','Achtung baby','09/12/1991','1','1U2');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('5','Once upon a time','10/10/1985','4','2SM');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('6','Good news F.N. world','12/11/1995','4','2SM');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('7','Sparkle in the rain','03/03/1984','4','2SM');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('8','Sister feelings call','04/03/1981','4','2SM');
insert into Disco(cod,nombre,fecha,cod_comp,cod_gru) values ('9','Living years','02/04/1988','5','3MM');

--Inserción de datos en Cancion.
--
insert into Cancion(cod,titulo,duracion) values (1,'20th Century Promise',4);
insert into Cancion(cod,titulo,duracion) values (2,'37 grados',4);
insert into Cancion(cod,titulo,duracion) values (3,'4th of July',3);
insert into Cancion(cod,titulo,duracion) values (4,'7 Deadly Sins',6);
insert into Cancion(cod,titulo,duracion) values (5,'A cara o cruz',5);
insert into Cancion(cod,titulo,duracion) values (6,'A sort of homecoming',3);
insert into Cancion(cod,titulo,duracion) values (7,'Afterglow',4);
insert into Cancion(cod,titulo,duracion) values (8,'Al atardecer',4);
insert into Cancion(cod,titulo,duracion) values (9,'Al sur',3);
insert into Cancion(cod,titulo,duracion) values (10,'Alive And Kicking',4);
insert into Cancion(cod,titulo,duracion) values (11,'All The things She..',4);
insert into Cancion(cod,titulo,duracion) values (12,'Alma de blues',4);
insert into Cancion(cod,titulo,duracion) values (13,'And The Band ...',4);
insert into Cancion(cod,titulo,duracion) values (14,'Andas junto a mí',3);
insert into Cancion(cod,titulo,duracion) values (15,'Annabel Lee',3);
insert into Cancion(cod,titulo,duracion) values (16,'Anything she does',3);
insert into Cancion(cod,titulo,duracion) values (17,'Artitoestoy',4);
insert into Cancion(cod,titulo,duracion) values (18,'Asoma el llanto',3);
insert into Cancion(cod,titulo,duracion) values (19,'Babyface',4);
insert into Cancion(cod,titulo,duracion) values (20,'Bad',2);
insert into Cancion(cod,titulo,duracion) values (21,'Barbara del campo',4);
insert into Cancion(cod,titulo,duracion) values (22,'Beautiful day',5);
insert into Cancion(cod,titulo,duracion) values (23,'Before',4);
insert into Cancion(cod,titulo,duracion) values (24,'Black and blue',3);
insert into Cancion(cod,titulo,duracion) values (25,'Blame',4);
insert into Cancion(cod,titulo,duracion) values (26,'Book of Brilliant...',5);
insert into Cancion(cod,titulo,duracion) values (27,'Brazilian',4);
insert into Cancion(cod,titulo,duracion) values (28,'Cada historia',3);
insert into Cancion(cod,titulo,duracion) values (29,'Can''t dance',4);
insert into Cancion(cod,titulo,duracion) values (30,'Careful In Career',4);
insert into Cancion(cod,titulo,duracion) values (31,'Carpet crawlers',4);
insert into Cancion(cod,titulo,duracion) values (32,'Cinema show',5);
insert into Cancion(cod,titulo,duracion) values (33,'Come A Long Way',2);
insert into Cancion(cod,titulo,duracion) values (34,'Como hemos cambiado',3);
insert into Cancion(cod,titulo,duracion) values (35,'Criminal World',5);
insert into Cancion(cod,titulo,duracion) values (36,'Cuando quiero sol',5);
insert into Cancion(cod,titulo,duracion) values (37,'Daddys Goma pay for',5);
insert into Cancion(cod,titulo,duracion) values (38,'Dance on a volcano',4);
insert into Cancion(cod,titulo,duracion) values (39,'De puntillas',3);
insert into Cancion(cod,titulo,duracion) values (40,'De sol a sol',4);
insert into Cancion(cod,titulo,duracion) values (41,'Dirty day',5);
insert into Cancion(cod,titulo,duracion) values (42,'Domino',5);
insert into Cancion(cod,titulo,duracion) values (43,'Don''t',4);
insert into Cancion(cod,titulo,duracion) values (44,'Dreaming while ...',4);
insert into Cancion(cod,titulo,duracion) values (45,'Driving the last...',4);
insert into Cancion(cod,titulo,duracion) values (46,'E.de C. instrumental',3);
insert into Cancion(cod,titulo,duracion) values (47,'East At Easter',4);
insert into Cancion(cod,titulo,duracion) values (48,'El canto del gallo',5);
insert into Cancion(cod,titulo,duracion) values (49,'El hombre de papel',2);
insert into Cancion(cod,titulo,duracion) values (50,'El nadador',3);
insert into Cancion(cod,titulo,duracion) values (51,'Elvis Presley & USA',3);
insert into Cancion(cod,titulo,duracion) values (52,'En Portugal',3);
insert into Cancion(cod,titulo,duracion) values (53,'En la oscuridad',5);
insert into Cancion(cod,titulo,duracion) values (54,'En un baile de perro',2);
insert into Cancion(cod,titulo,duracion) values (55,'Encadenada',4);
insert into Cancion(cod,titulo,duracion) values (56,'Escuela de calor',3);
insert into Cancion(cod,titulo,duracion) values (57,'Even Better Than...',3);
insert into Cancion(cod,titulo,duracion) values (58,'Every body gets...',5);
insert into Cancion(cod,titulo,duracion) values (59,'Fading lights',5);
insert into Cancion(cod,titulo,duracion) values (60,'Fire',4);
insert into Cancion(cod,titulo,duracion) values (61,'Firth of fith',4);
insert into Cancion(cod,titulo,duracion) values (62,'Fly',4);
insert into Cancion(cod,titulo,duracion) values (63,'Get up',4);
insert into Cancion(cod,titulo,duracion) values (64,'Ghostdancing',3);
insert into Cancion(cod,titulo,duracion) values (65,'Gloria',3);
insert into Cancion(cod,titulo,duracion) values (67,'Great Leap Forward',4);
insert into Cancion(cod,titulo,duracion) values (68,'Guitarra y voz',1);
insert into Cancion(cod,titulo,duracion) values (69,'Hadaly',3);
insert into Cancion(cod,titulo,duracion) values (70,'Hay algo raro...',3);
insert into Cancion(cod,titulo,duracion) values (71,'Historia de playback',3);
insert into Cancion(cod,titulo,duracion) values (72,'Hypnotised',5);
insert into Cancion(cod,titulo,duracion) values (73,'I Fall Down',4);
insert into Cancion(cod,titulo,duracion) values (74,'I Threw a Brick',3);
insert into Cancion(cod,titulo,duracion) values (75,'I Wish You Were Here',4);
insert into Cancion(cod,titulo,duracion) values (76,'I know what I like',3);
insert into Cancion(cod,titulo,duracion) values (77,'Icaro',3);
insert into Cancion(cod,titulo,duracion) values (78,'In the glow of night',4);
insert into Cancion(cod,titulo,duracion) values (79,'In too deep',4);
insert into Cancion(cod,titulo,duracion) values (80,'Indian summer sky',4);
insert into Cancion(cod,titulo,duracion) values (81,'Invisible touch',3);
insert into Cancion(cod,titulo,duracion) values (82,'Is That All',3);
insert into Cancion(cod,titulo,duracion) values (83,'Jesus he knows me',5);
insert into Cancion(cod,titulo,duracion) values (84,'La futura promesa',3);
insert into Cancion(cod,titulo,duracion) values (85,'La ley',4);
insert into Cancion(cod,titulo,duracion) values (86,'La mala hora',3);
insert into Cancion(cod,titulo,duracion) values (87,'La negra flor',5);
insert into Cancion(cod,titulo,duracion) values (88,'La noche',2);
insert into Cancion(cod,titulo,duracion) values (89,'La secta del mar',3);
insert into Cancion(cod,titulo,duracion) values (90,'Lamb lies down...',5);
insert into Cancion(cod,titulo,duracion) values (91,'Land of confusion',4);
insert into Cancion(cod,titulo,duracion) values (92,'League Of nations',4);
insert into Cancion(cod,titulo,duracion) values (93,'Lemon',6);
insert into Cancion(cod,titulo,duracion) values (94,'Let''s pretend...',5);
insert into Cancion(cod,titulo,duracion) values (95,'Living forever',4);
insert into Cancion(cod,titulo,duracion) values (96,'Living years',4);
insert into Cancion(cod,titulo,duracion) values (97,'Llovió',2);
insert into Cancion(cod,titulo,duracion) values (98,'Lluvia del porvenir',3);
insert into Cancion(cod,titulo,duracion) values (99,'Los endos',4);
insert into Cancion(cod,titulo,duracion) values (100,'Love is Blindness',4);
insert into Cancion(cod,titulo,duracion) values (101,'Luna de agosto',3);
insert into Cancion(cod,titulo,duracion) values (102,'MLK',2);
insert into Cancion(cod,titulo,duracion) values (103,'Me das el mar',3);
insert into Cancion(cod,titulo,duracion) values (104,'Mil mariposas',3);
insert into Cancion(cod,titulo,duracion) values (105,'Moon Cry Like A Baby',4);
insert into Cancion(cod,titulo,duracion) values (106,'Musical box',4);
insert into Cancion(cod,titulo,duracion) values (107,'My Life',4);
insert into Cancion(cod,titulo,duracion) values (108,'My crime of passion',3);
insert into Cancion(cod,titulo,duracion) values (109,'Mysterious Ways',4);
insert into Cancion(cod,titulo,duracion) values (110,'Nada tiene sentido',3);
insert into Cancion(cod,titulo,duracion) values (111,'Never a time',5);
insert into Cancion(cod,titulo,duracion) values (112,'Ni tu ni yo',3);
insert into Cancion(cod,titulo,duracion) values (113,'Night Music',4);
insert into Cancion(cod,titulo,duracion) values (114,'No hay humor',3);
insert into Cancion(cod,titulo,duracion) values (115,'No hay palabras',3);
insert into Cancion(cod,titulo,duracion) values (116,'No son of mine',4);
insert into Cancion(cod,titulo,duracion) values (117,'Nobody knows',3);
insert into Cancion(cod,titulo,duracion) values (118,'Nobody''s perfect',3);
insert into Cancion(cod,titulo,duracion) values (119,'Numb',4);
insert into Cancion(cod,titulo,duracion) values (120,'October',3);
insert into Cancion(cod,titulo,duracion) values (121,'Oh Jungleland',3);
insert into Cancion(cod,titulo,duracion) values (122,'Once Upon A Time',4);
insert into Cancion(cod,titulo,duracion) values (123,'One',4);
insert into Cancion(cod,titulo,duracion) values (124,'Oscuro affaire',3);
insert into Cancion(cod,titulo,duracion) values (125,'Poor boy down',2);
insert into Cancion(cod,titulo,duracion) values (126,'Price',1);
insert into Cancion(cod,titulo,duracion) values (127,'Promenade',3);
insert into Cancion(cod,titulo,duracion) values (128,'Recibes cartas',3);
insert into Cancion(cod,titulo,duracion) values (130,'Rejoice',4);
insert into Cancion(cod,titulo,duracion) values (131,'Rio Po',4);
insert into Cancion(cod,titulo,duracion) values (132,'Robbery, assault...',3);
insert into Cancion(cod,titulo,duracion) values (133,'Sanctify yourself',4);
insert into Cancion(cod,titulo,duracion) values (134,'Scarlet',4);
insert into Cancion(cod,titulo,duracion) values (135,'Sed de amor',4);
insert into Cancion(cod,titulo,duracion) values (136,'Seeing is believing',5);
insert into Cancion(cod,titulo,duracion) values (137,'Semilla negra',4);
insert into Cancion(cod,titulo,duracion) values (138,'Sentir tu calor',4);
insert into Cancion(cod,titulo,duracion) values (139,'Ser de agua',3);
insert into Cancion(cod,titulo,duracion) values (140,'Shake Of The Ghosts',3);
insert into Cancion(cod,titulo,duracion) values (141,'She''s A river',3);
insert into Cancion(cod,titulo,duracion) values (142,'Since I lost you',4);
insert into Cancion(cod,titulo,duracion) values (143,'So Cruel',6);
insert into Cancion(cod,titulo,duracion) values (144,'Some days are better',4);
insert into Cancion(cod,titulo,duracion) values (145,'Sound In 70 Cities',4);
insert into Cancion(cod,titulo,duracion) values (146,'Speed Your Love...',4);
insert into Cancion(cod,titulo,duracion) values (147,'Squok',4);
insert into Cancion(cod,titulo,duracion) values (148,'Stay',4);
insert into Cancion(cod,titulo,duracion) values (149,'Stop baby',4);
insert into Cancion(cod,titulo,duracion) values (150,'Stranger in a Land',4);
insert into Cancion(cod,titulo,duracion) values (151,'Street Hassle',4);
insert into Cancion(cod,titulo,duracion) values (152,'Supper''s ready',5);
insert into Cancion(cod,titulo,duracion) values (153,'Tell me why',5);
insert into Cancion(cod,titulo,duracion) values (154,'The Kick Inside ofme',4);
insert into Cancion(cod,titulo,duracion) values (155,'The american',4);
insert into Cancion(cod,titulo,duracion) values (156,'The first time',3);
insert into Cancion(cod,titulo,duracion) values (157,'The last domino',5);
insert into Cancion(cod,titulo,duracion) values (158,'The unforgettable fi',5);
insert into Cancion(cod,titulo,duracion) values (159,'The wanderer',4);
insert into Cancion(cod,titulo,duracion) values (160,'Theme For Great city',3);
insert into Cancion(cod,titulo,duracion) values (161,'This Time',3);
insert into Cancion(cod,titulo,duracion) values (162,'Throwing it all away',4);
insert into Cancion(cod,titulo,duracion) values (163,'Time and place',4);
insert into Cancion(cod,titulo,duracion) values (164,'Tomorrow',3);
insert into Cancion(cod,titulo,duracion) values (165,'Tonight, tonight...',4);
insert into Cancion(cod,titulo,duracion) values (166,'Tormenta de arena',4);
insert into Cancion(cod,titulo,duracion) values (167,'Tryin to Throw...',4);
insert into Cancion(cod,titulo,duracion) values (168,'Ultra Violet',5);
insert into Cancion(cod,titulo,duracion) values (169,'Un africano...',3);
insert into Cancion(cod,titulo,duracion) values (170,'Until The end...',5);
insert into Cancion(cod,titulo,duracion) values (171,'Up On The Catwalk',4);
insert into Cancion(cod,titulo,duracion) values (172,'Volviendo a casa',3);
insert into Cancion(cod,titulo,duracion) values (173,'Waterfront',3);
insert into Cancion(cod,titulo,duracion) values (174,'Way of the world',5);
insert into Cancion(cod,titulo,duracion) values (175,'Way you look at me',3);
insert into Cancion(cod,titulo,duracion) values (176,'White Hot Day',5);
insert into Cancion(cod,titulo,duracion) values (177,'Whos Gonna ride...',5);
insert into Cancion(cod,titulo,duracion) values (178,'Why me?',3);
insert into Cancion(cod,titulo,duracion) values (179,'Wire',2);
insert into Cancion(cod,titulo,duracion) values (180,'With a Shout',4);
insert into Cancion(cod,titulo,duracion) values (181,'Wonderful In Young',5);
insert into Cancion(cod,titulo,duracion) values (182,'Word of mouth',3);
insert into Cancion(cod,titulo,duracion) values (183,'Yesterday, today,..',2);
insert into Cancion(cod,titulo,duracion) values (184,'Zoo Station',4);
insert into Cancion(cod,titulo,duracion) values (185,'Zooropa',6);
insert into Cancion(cod,titulo,duracion) values (186,'Hold on my heart',4);

--
--Inserción de datos en Esta
--
insert into Esta(can,cod) values (1,'8');
insert into Esta(can,cod) values (2,'18');
insert into Esta(can,cod) values (3,'3');
insert into Esta(can,cod) values (4,'6');
insert into Esta(can,cod) values (5,'18');
insert into Esta(can,cod) values (6,'3');
insert into Esta(can,cod) values (7,'13');
insert into Esta(can,cod) values (8,'15');
insert into Esta(can,cod) values (9,'14');
insert into Esta(can,cod) values (10,'5');
insert into Esta(can,cod) values (11,'5');
insert into Esta(can,cod) values (12,'16');
insert into Esta(can,cod) values (13,'6');
insert into Esta(can,cod) values (14,'15');
insert into Esta(can,cod) values (15,'18');
insert into Esta(can,cod) values (16,'12');
insert into Esta(can,cod) values (17,'4');
insert into Esta(can,cod) values (18,'16');
insert into Esta(can,cod) values (19,'2');
insert into Esta(can,cod) values (20,'3');
insert into Esta(can,cod) values (21,'15');
insert into Esta(can,cod) values (22,'9');
insert into Esta(can,cod) values (23,'10');
insert into Esta(can,cod) values (24,'9');
insert into Esta(can,cod) values (25,'9');
insert into Esta(can,cod) values (26,'7');
insert into Esta(can,cod) values (27,'12');
insert into Esta(can,cod) values (28,'16');
insert into Esta(can,cod) values (29,'11');
insert into Esta(can,cod) values (30,'8');
insert into Esta(can,cod) values (31,'13');
insert into Esta(can,cod) values (32,'13');
insert into Esta(can,cod) values (33,'5');
insert into Esta(can,cod) values (34,'15');
insert into Esta(can,cod) values (35,'6');
insert into Esta(can,cod) values (36,'15');
insert into Esta(can,cod) values (37,'2');
insert into Esta(can,cod) values (38,'13');
insert into Esta(can,cod) values (39,'15');
insert into Esta(can,cod) values (40,'14');
insert into Esta(can,cod) values (41,'2');
insert into Esta(can,cod) values (42,'12');
insert into Esta(can,cod) values (43,'9');
insert into Esta(can,cod) values (44,'11');
insert into Esta(can,cod) values (45,'11');
insert into Esta(can,cod) values (46,'17');
insert into Esta(can,cod) values (47,'7');
insert into Esta(can,cod) values (48,'18');
insert into Esta(can,cod) values (49,'18');
insert into Esta(can,cod) values (50,'17');
insert into Esta(can,cod) values (51,'3');
insert into Esta(can,cod) values (52,'17');
insert into Esta(can,cod) values (53,'14');
insert into Esta(can,cod) values (54,'18');
insert into Esta(can,cod) values (55,'16');
insert into Esta(can,cod) values (56,'17');
insert into Esta(can,cod) values (57,'4');
insert into Esta(can,cod) values (58,'10');
insert into Esta(can,cod) values (59,'11');
insert into Esta(can,cod) values (60,'1');
insert into Esta(can,cod) values (61,'13');
insert into Esta(can,cod) values (62,'4');
insert into Esta(can,cod) values (63,'10');
insert into Esta(can,cod) values (64,'5');
insert into Esta(can,cod) values (65,'1');
insert into Esta(can,cod) values (67,'6');
insert into Esta(can,cod) values (68,'16');
insert into Esta(can,cod) values (69,'17');
insert into Esta(can,cod) values (70,'14');
insert into Esta(can,cod) values (71,'17');
insert into Esta(can,cod) values (72,'6');
insert into Esta(can,cod) values (73,'1');
insert into Esta(can,cod) values (74,'1');
insert into Esta(can,cod) values (75,'5');
insert into Esta(can,cod) values (76,'13');
insert into Esta(can,cod) values (77,'15');
insert into Esta(can,cod) values (78,'12');
insert into Esta(can,cod) values (79,'12');
insert into Esta(can,cod) values (80,'3');
insert into Esta(can,cod) values (81,'12');
insert into Esta(can,cod) values (82,'1');
insert into Esta(can,cod) values (83,'11');
insert into Esta(can,cod) values (84,'16');
insert into Esta(can,cod) values (85,'17');
insert into Esta(can,cod) values (86,'18');
insert into Esta(can,cod) values (87,'18');
insert into Esta(can,cod) values (88,'16');
insert into Esta(can,cod) values (89,'17');
insert into Esta(can,cod) values (90,'13');
insert into Esta(can,cod) values (91,'12');
insert into Esta(can,cod) values (92,'8');
insert into Esta(can,cod) values (93,'2');
insert into Esta(can,cod) values (94,'10');
insert into Esta(can,cod) values (95,'11');
insert into Esta(can,cod) values (96,'9');
insert into Esta(can,cod) values (97,'15');
insert into Esta(can,cod) values (98,'18');
insert into Esta(can,cod) values (99,'13');
insert into Esta(can,cod) values (100,'4');
insert into Esta(can,cod) values (101,'18');
insert into Esta(can,cod) values (102,'3');
insert into Esta(can,cod) values (103,'16');
insert into Esta(can,cod) values (104,'15');
insert into Esta(can,cod) values (105,'7');
insert into Esta(can,cod) values (106,'13');
insert into Esta(can,cod) values (107,'6');
insert into Esta(can,cod) values (108,'10');
insert into Esta(can,cod) values (109,'4');
insert into Esta(can,cod) values (110,'14');
insert into Esta(can,cod) values (111,'11');
insert into Esta(can,cod) values (112,'14');
insert into Esta(can,cod) values (113,'6');
insert into Esta(can,cod) values (114,'16');
insert into Esta(can,cod) values (115,'16');
insert into Esta(can,cod) values (116,'11');
insert into Esta(can,cod) values (117,'9');
insert into Esta(can,cod) values (118,'9');
insert into Esta(can,cod) values (119,'2');
insert into Esta(can,cod) values (120,'1');
insert into Esta(can,cod) values (121,'5');
insert into Esta(can,cod) values (122,'5');
insert into Esta(can,cod) values (123,'4');
insert into Esta(can,cod) values (124,'17');
insert into Esta(can,cod) values (125,'9');
insert into Esta(can,cod) values (126,'3');
insert into Esta(can,cod) values (127,'3');
insert into Esta(can,cod) values (128,'15');
insert into Esta(can,cod) values (130,'1');
insert into Esta(can,cod) values (131,'16');
insert into Esta(can,cod) values (132,'13');
insert into Esta(can,cod) values (133,'5');
insert into Esta(can,cod) values (134,'1');
insert into Esta(can,cod) values (135,'14');
insert into Esta(can,cod) values (136,'9');
insert into Esta(can,cod) values (137,'17');
insert into Esta(can,cod) values (138,'15');
insert into Esta(can,cod) values (139,'15');
insert into Esta(can,cod) values (140,'7');
insert into Esta(can,cod) values (141,'6');
insert into Esta(can,cod) values (142,'11');
insert into Esta(can,cod) values (143,'4');
insert into Esta(can,cod) values (144,'2');
insert into Esta(can,cod) values (145,'8');
insert into Esta(can,cod) values (146,'7');
insert into Esta(can,cod) values (147,'13');
insert into Esta(can,cod) values (148,'2');
insert into Esta(can,cod) values (149,'10');
insert into Esta(can,cod) values (150,'1');
insert into Esta(can,cod) values (151,'7');
insert into Esta(can,cod) values (152,'13');
insert into Esta(can,cod) values (153,'11');
insert into Esta(can,cod) values (154,'7');
insert into Esta(can,cod) values (155,'8');
insert into Esta(can,cod) values (156,'2');
insert into Esta(can,cod) values (157,'12');
insert into Esta(can,cod) values (158,'3');
insert into Esta(can,cod) values (159,'2');
insert into Esta(can,cod) values (160,'8');
insert into Esta(can,cod) values (161,'6');
insert into Esta(can,cod) values (162,'12');
insert into Esta(can,cod) values (163,'10');
insert into Esta(can,cod) values (164,'1');
insert into Esta(can,cod) values (165,'12');
insert into Esta(can,cod) values (166,'17');
insert into Esta(can,cod) values (167,'4');
insert into Esta(can,cod) values (168,'4');
insert into Esta(can,cod) values (169,'17');
insert into Esta(can,cod) values (170,'4');
insert into Esta(can,cod) values (171,'7');
insert into Esta(can,cod) values (172,'14');
insert into Esta(can,cod) values (173,'7');
insert into Esta(can,cod) values (174,'11');
insert into Esta(can,cod) values (175,'10');
insert into Esta(can,cod) values (176,'7');
insert into Esta(can,cod) values (177,'4');
insert into Esta(can,cod) values (178,'9');
insert into Esta(can,cod) values (179,'3');
insert into Esta(can,cod) values (180,'1');
insert into Esta(can,cod) values (181,'8');
insert into Esta(can,cod) values (182,'10');
insert into Esta(can,cod) values (183,'10');
insert into Esta(can,cod) values (184,'4');
insert into Esta(can,cod) values (185,'2');
insert into Esta(can,cod) values (186,'11');