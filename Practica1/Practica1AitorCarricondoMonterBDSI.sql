/*1. Obtener el código, el nombre, la dirección y el fax de todas las compañías hay.*/

-- SELECT cod, nombre, dir, fax FROM companyia;



/*2. Obtener el dni y el nombre de los artistas cuya edad sea menor o igual que 25 años.*/

-- SELECT dni, nombre FROM artista WHERE edad <= 25;



/*3. Obtener el nombre y país de todos los grupos de estilo relacionado con Pop.*/

-- SELECT nombre, pais FROM grupo WHERE estilo = 'POP';



/*4. Obtener el valor del atributo sede de aquellos clubs de fans en los que el número de socios coincida 
con el número de socias*/

-- SELECT sede FROM club WHERE num_socios = num_socias;



/*5. ¿Cuántos grupos hay?. */

-- SELECT Count(*) FROM grupo;



/*6. ¿Cuántos artista hay con edad superior a 25 años?*/

-- SELECT dni, nombre, edad FROM artista WHERE edad > 25;



/*7. Obtener la media de edad de los artistas*/

-- SELECT SUM(edad)/Count(*) FROM artista;



/*8. Obtener el número mínimo de socios y el número máximo de socias de los clubs existentes.*/ 

-- SELECT MAX(num_socias), MIN(num_socios) FROM club;



/*9. ¿Cuántos discos hay del grupo cuyo código es 1U2?*/

-- SELECT Count(*) FROM disco WHERE cod_gru = '1U2';



/*10. Selecciona el nombre de los grupos que no sean de España.*/

-- SELECT nombre FROM grupo WHERE pais != 'España';



/*11. Obtener el título de las canciones con más de 5 minutos de duración*/

-- SELECT titulo FROM cancion WHERE duracion > 5;



/*12. Según los datos en la base de datos, obtener la lista de las distintas funciones que se pueden 
realizar en un grupo. */

-- SELECT distinct funcion FROM pertenece;



/*13. Selecciona el nombre y la sede de los clubes de fans con más de 500 socios*/

-- SELECT nombre, sede FROM club WHERE (num_socias + num_socios) > 500;



/*14. Obtener el nombre y la sede de cada club de fans de grupos de España así como el nombre del 
grupo al que admiran.*/

-- SELECT club.nombre, club.sede, grupo.nombre FROM club, grupo WHERE (club.cod_gru = grupo.cod) and (grupo.pais = 'España');



/*15. Obtener el nombre de los discos grabados por el grupo 'Genesis'.*/

-- SELECT disco.nombre FROM disco, grupo WHERE (grupo.nombre = 'Genesis') and (disco.cod_gru = grupo.cod);



/*16. Obtener los nombres de compañías y direcciones postales de aquellas compañías que han grabado 
algún disco que empiece por 'A'.*/

-- SELECT companyia.nombre, companyia.dir FROM companyia, disco 
-- WHERE (disco.nombre like 'A%') and (disco.cod_comp = companyia.cod);



/*17. Obtener el nombre de los artistas que pertenezcan a un grupo de España. */

-- SELECT artista.nombre FROM grupo, pertenece, artista 
-- WHERE (pertenece.cod = grupo.cod) and (artista.dni = pertenece.dni) and (grupo.pais = 'España');



/*18. Obtener el nombre de los discos que contienen alguna canción que dure más de 5 minutos.*/

-- SELECT distinct disco.nombre FROM disco, cancion WHERE (cancion.duracion > 5);



/*19. Obtener los nombres de las canciones que dan nombre al disco en el que aparecen.*/

-- SELECT cancion.titulo FROM disco, cancion WHERE cancion.titulo = disco.nombre;



/*20. Obtener el nombre de los discos grabados por el grupo 'Genesis', y el nombre de la compañia que
grabó el disco.*/

-- SELECT disco.nombre, companyia.nombre FROM companyia ,disco, grupo WHERE (grupo.nombre = 'Genesis') and (disco.cod_gru = grupo.cod) and (disco.cod_comp = companyia.cod);



/*21. Obtener nombre y estilo del grupo al que pertenezca algún artista mayor de 45 años.*/

-- SELECT grupo.nombre, grupo.estilo FROM grupo, pertenece, artista WHERE (artista.edad > 45) and (pertenece.cod = grupo.cod) and (artista.dni = pertenece.dni);




