/*40. Obtener un listado de los grupos con clubs de fans, indicando el nombre del club de fans y el
nombre del grupo.*/

-- SELECT distinct club.nombre, grupo.nombre FROM Grupo LEFT JOIN Club ON grupo.cod = club.cod_gru;

/*41. Obtener un listado de los grupos indicando cuantos clubs de fans tiene cada uno.*/

-- SELECT grupo.nombre, count(club.cod_gru) FROM Club JOIN Grupo ON grupo.cod = club.cod_gru
-- GROUP BY grupo.cod;

/*42. Obtener para aquellas compañias que han grabado algún disco, el nombre de la compañia y el titulo
del disco grabado.*/

-- SELECT companyia.nombre, disco.nombre FROM Companyia LEFT JOIN Disco ON disco.cod_comp = companyia.cod;

/*43. Obtener para todas las compañías existentes, el número de discos que ha grabado.(Deben salir
todas las compañias).*/

-- SELECT companyia.nombre, count(disco.cod_comp) FROM Disco RIGHT JOIN Companyia ON companyia.cod = disco.cod_comp 
-- GROUP BY companyia.cod;

/*44. Obtener el codigo del grupo y la edad media,de aquellos grupo de mayor media de edad de todos
los grupos.*/

-- SELECT grupo.cod, round(avg(artista.edad)) FROM Grupo, Pertenece, Artista WHERE grupo.cod = pertenece.cod and pertenece.dni = artista.dni
-- GROUP BY grupo.cod HAVING avg(artista.edad) > all(SELECT avg(artista.edad) FROM Artista);


/*45. Obtener el nombre de los artistas que pertenecen a más de un grupo.*/

-- SELECT artista.nombre FROM Grupo, Artista, Pertenece ;

/*46. Añadir el siguiente grupo de USA: ‘No Doubt’ con codigo ‘ND’.*/

-- INSERT INTO Grupo (cod,nombre) values ('7ND', 'No Doubt');

/*47. Insertar los siguientes integrantes del grupo: 
 - Gwen Stefani con dni,21212365D : vocalista
 - Tony Kanal con edad 33, y dni 22365487G : bajista
 - Tom Dumont con edad 21 y dni 11234568H : guitarrista*/
  
 -- INSERT INTO Artista (dni, nombre) Values ('21212365D','Gwen Stefani');
 -- INSERT INTO Pertenece (dni, cod, funcion) Values ('21212365D','7ND','Vocalista');7
 
 -- INSERT INTO Artista (dni, nombre, edad) Values ('22365487G','Tony Kanal','33');
 -- INSERT INTO Pertenece (dni, cod, funcion) Values ('22365487G','7ND','Bajista');
 
 -- INSERT INTO Artista (dni, nombre, edad) Values ('11234568H','Tom Dumont','21');
 -- INSERT INTO Pertenece (dni, cod, funcion) Values ('11234568H','7ND','Guitarrista');
 
 
 /*48. Indicar la siguiente fecha de creación del grupo ‘No Doubt’: 05/04/1996.*/
 
 -- INSERT INTO Grupo (fecha) Values ('1996-04-05 00:00:00');
 
 /*49. Añadir un minuto más de duración a todas las canciones. */
 
 -- UPDATE Cancion SET duracion = '5.00' WHERE duracion = '4.00';
 
 /*50. Disminuir en un minuto la duración de todas las canciones del grupo ‘U2’. */
 
 -- UPDATE Cancion SET duracion = (duracion - 1) WHERE (SELECT nombre FROM Grupo WHERE nombre = 'U2');
 
 /*51. Eliminar el grupo introducido. ¿Se puede borrar? ¿ por qué? ¿qué se podría haber hecho?*/



 
 
 
 
 