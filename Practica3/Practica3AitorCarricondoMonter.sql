/*33. Obtener un listado donde aparezca el codigo del grupo y el número de componentes que lo integran.*/

-- SELECT grupo.cod, count(pertenece.cod = grupo.cod) FROM pertenece, artista, grupo WHERE (artista.dni = pertenece.dni) and (pertenece.cod = grupo.cod) GROUP BY grupo.cod;


/*34. Obtener el número de discos de cada grupo, indicando el nombre del grupo y el total de discos
grabados.*/

-- SELECT grupo.nombre, count(disco.cod_gru = grupo.cod) FROM disco, grupo WHERE (disco.cod_gru = grupo.cod) GROUP BY grupo.nombre ORDER BY grupo.nombre DESC;


/*35. Obtener un listado de los nombres de grupos con el número de componentes que lo integran.*/

-- SELECT grupo.nombre, count(pertenece.cod = grupo.cod) FROM grupo, artista, pertenece WHERE (artista.dni = pertenece.dni) and (pertenece.cod = grupo.cod) GROUP BY grupo.nombre ORDER BY grupo.nombre DESC;


/*36. Obtener para cada grupo de más de dos componentes, el nombre del grupo y el número de
componentes. (resultado debe aparecer ordenado alfabeticamente en orden inverso. (Z..A)*/

-- SELECT grupo.nombre, count(pertenece.cod = grupo.cod) FROM grupo, artista, pertenece 
-- WHERE (artista.dni = pertenece.dni) and (pertenece.cod = grupo.cod) GROUP BY grupo.nombre
-- HAVING count(pertenece.cod = grupo.cod) > 2 ORDER BY grupo.nombre DESC;

/*37. Obtener el nombre de los grupos de España y la suma de sus fans.*/

-- SELECT grupo.nombre, (club.num_socios + club.num_socias) FROM grupo, club WHERE (grupo.cod = club.cod_gru) and grupo.pais = 'España' GROUP BY grupo.nombre;


/*38. Obtener el nombre de los artistas de aquellos grupos que tienen menos de 3 integrantes.*/

/*SELECT * FROM artista, pertenece WHERE pertenece.cod IN (*/SELECT count(pertenece.cod) < 3 FROM pertenece, grupo GROUP BY grupo.cod;


/*39. Nombre de las compañias que han grabado más de 3 discos. Obtener el resultado ordenado
alfabéticamente por el nombre.*/

