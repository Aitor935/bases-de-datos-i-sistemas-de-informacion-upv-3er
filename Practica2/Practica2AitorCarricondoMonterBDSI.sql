/*22. Obtener nombre de los discos del grupo más viejo.*/

-- SELECT disco.nombre FROM disco, grupo WHERE (disco.cod_gru = grupo.cod) and (grupo.fecha <= all(SELECT min(grupo.fecha) FROM grupo));

/*23. Obtener el nombre de los discos grabados por clubs de fans con más de 5000 personas.*/

-- SELECT distinct disco.nombre FROM Disco, club WHERE (club.num_socias + club.num_socios) > all(select from club where club.num_socias + club.num_socios > 5000);


/*24. Obtener el nombre de los clubes con mayor número de fans masculinos, indicando ese número.*/

-- SELECT club.nombre, club.num_socios FROM club WHERE club.num_socios >= all (select club.num_socios from club);


/*25. Obtener el título de las canciones que duran 1 minuto más que la de menor duración.*/

-- SELECT cancion.titulo FROM cancion WHERE ;


/*26. Obtener el nombre de los grupos sin clubs de fans.*/

-- SELECT grupo.nombre FROM grupo, club WHERE NOT EXISTS(SELECT club.num_socios, club.num_socias FROM club);


/*27. Obtener la edad media de los artistas que pertenecen a grupos de POP.*/

-- SELECT round(avg(artista.edad)) FROM artista, grupo, pertenece WHERE (SELECT (pertenece.cod = grupo.cod) and (artista.dni = pertenece.dni) and grupo.estilo = 'POP');


/*28. Obtener el nombre de los artistas con una edad superior a la edad media de todos los artistas.*/

-- SELECT artista.nombre FROM artista WHERE artista.edad > (SELECT avg(artista.edad) FROM artista);


/*29. Obtener el nombre de los grupos que tienen el club con menor número de fans femeninas.*/

-- SELECT * FROM grupo, club WHERE club.num_socias <= all(SELECT min(club.num_socias) FROM club);


/*30. Obtener el nombre del artista más viejo.*/

-- SELECT artista.nombre FROM artista WHERE artista.edad = all(select max(artista.edad) FROM artista);


/*31. Obtener el nombre del artista y la edad del más mayor de cada grupo.*/

-- SELECT artista.nombre, artista.edad FROM artista, grupo WHERE artista.edad >= any(SELECT max(artista.edad) FROM artista, pertenece WHERE (pertenece.cod = grupo.cod));


/*32. Nombre de las compañías que han grabado más de 3 discos. Obtener el resultado ordenado
alfabéticamente por el nombre*/

-- SELECT companyia.nombre FROM companyia, disco WHERE companyia.nombre = (SELECT companyia.nombre FROM companyia, disco WHERE  ) ORDER BY companyia.nombre ASC;